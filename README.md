# éléments d'identité visuelle - RFF

Ce dépôt contient des éléments d'identité visuelle pour le [Rassemblement des Fablab Français (RFF)](https://fablab.fr).

## élément graphique

Cet élément n'est formé que d'une seule ligne qui boucle sur elle-même pour former un R stylisé.

L'élément est un dessin technique créé avec le logiciel libre [FreeCad](https://ffreecadweb.org/)

![Cooper Hewitt Interpol](images/freecad-2.png)

Il est donc possible de créer de nouvelles variantes du symbole graphique.

## couleurs

Les couleurs sont celles traditionnelles des fab-lab, légèrement modifiées pour se rapprocher de celles du drapeau français.

* bleu : rgb(19,85,216)
* rouge : rgb(216,25,39)
* vert : rgb(0,146,69)

## typographie

La typographie proposée est la [Cooper Hewitt Interpol](https://framagit.org/martin-leo/cooperhewitt-interpol-typeface) ([voir une démonstration en ligne](http://cooper-hewitt-interpol.leomartin.net/)), une modification de la typographie libre créé par Chester Jenkins pour le Cooper Hewitt Museum.

![Cooper Hewitt Interpol](images/images-01.png)

La principale caractéristique de cette typographie est d'avoir été débarrassée de toute courbe. Elle a été proposé en raison de cet aspect.

![Cooper Hewitt Interpol](images/images-02.png)

La [typographie originale](https://github.com/cooperhewitt/cooperhewitt-typeface/) peut bien sûr être utilisée en complément.

## fichiers contenus dans ce dépôt

* fichier source du symbole graphique au format FreeCad
* export svg de variation du symbole graphique
* export svg de compositions

> La typographie Cooper Hewitt Interpol est quand à elle contenue dans [son propre dépôt](https://framagit.org/martin-leo/cooperhewitt-interpol-typeface).
